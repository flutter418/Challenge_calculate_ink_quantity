# ink_calculate

Project developed with dart framework flutter language.
To run the project, the following steps or versions are required:
Step 1:
• Dart version 2.18.5
• DevTools version 2.15.0
Step 2:
Clone the repository and run the flutter pub get command to install the project's dependencies.
Step 3:
Run the project on your mobile from any model or virtual machine.

## known bugs

Fine tuning buttons are not working correctly when decreasing height and width.
